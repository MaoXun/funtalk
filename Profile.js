var profile_ref = db.collection('Profile');
var newWidth = 0;
var newHeight = 0;
var profile_id = localStorage.getItem("profile_id");
var canvas_dict = {
    "check-head-pic": "preview-canvas-head",
    "check-back-pic": "preview-canvas-back"
};
var modal_body_dict = {
    "check-head-pic": "modal-body-head",
    "check-back-pic": "modal-body-back"
};

$('document').ready(function () {
    if (sessionStorage.getItem("original-focus-icon")) {
        focusIcon(sessionStorage.getItem("original-focus-icon"));
    }
    focusIcon("profile-icon");

    $('.single-item').slick({
        dots: true
    });

    //給第二張slider高度
    let sliderHeight = document.getElementsByClassName("slick-list")[0].offsetHeight;
    document.getElementById("slick-info-1").setAttribute("style", `height: ${sliderHeight}px;`);

    //set headpic and backgroundimg and discription (realtime)
    profile_ref.doc(profile_id).onSnapshot(doc => {
        document.getElementById("head-pic").setAttribute("style", `background-image: url("${doc.data().head_pic}")`);
        document.getElementById("background-pic").setAttribute("style", `background-image: url("${doc.data().background_pic}")`);
        document.getElementById("big-name").innerText = doc.data().name;
        document.getElementById("text-content").innerText = doc.data().discription;
        displaySecondIntro(doc.data().friends);

        let loaders = document.getElementsByClassName("loader-loading");
        for (let i = 0; i < loaders.length; i++) {
            loaders[i].setAttribute("style", "display: none;");
        }
    });

    //給上傳圖片功能
    let preview;
    let input = document.getElementById("uploadFile");

    input.addEventListener('change', function () {
        preview = document.getElementById(imgtype);
        let files = document.getElementById("uploadFile").files; //read only
        let reader = new FileReader();
        reader.readAsDataURL(files[0]);    // 解析成base64格式
        reader.onload = function () {
            //console.log(this.result);
            base64string = this.result;        // 解析後的資料           
            preview.setAttribute("src", base64string);

            //等待照片跑完 (onload是非同步)
            preview.onload = function () {
                let ratio = preview.width / preview.height;
                let modal = document.getElementById(modal_body_dict[imgtype]);
                if (preview.width >= preview.height && preview.width > modal.offsetWidth) //只關心寬度
                {
                    imgWidth = modal.offsetWidth - 20;
                    imgHeight = Math.ceil(imgWidth / ratio);
                } else if (preview.height > preview.width && preview.height > modal.offsetWidth) {
                    imgHeight = modal.offsetWidth - 20;
                    imgWidth = Math.ceil(imgHeight * ratio);
                } else {
                    imgWidth = preview.width;
                    imgHeight = preview.height;
                }

                newWidth = imgWidth;
                newHeight = imgHeight;
                drawCanvas(preview, canvas_dict[imgtype]); //要放裡面
            }
        };
    });

});

var imgtype = "";
var imgWidth = 0;
var imgHeight = 0;
function checkHeadPic() {
    imgtype = "check-head-pic";
    document.getElementById("setting-page").setAttribute("style", "display:none;"); //避免因absolute而錯位
    //執行(非同步)
    profile_ref.onSnapshot(querySnapshot => {
        querySnapshot.forEach(function (doc) {
            if (doc.id === profile_id) {
                let img = document.getElementById("check-head-pic");
                img.setAttribute("src", doc.data().head_pic);

                //等照片跑完 且onload為非同步
                img.onload = function () {
                    imgWidth = img.width;
                    imgHeight = img.height;

                    drawCanvas(img, "preview-canvas-head");
                    return;
                }
            }
        });
    });

    $('#checkheadimg').on('hidden.bs.modal', function (e) {
        document.getElementById("setting-page").setAttribute("style", "display:block;");
    });
}

function checkBackPic() {
    imgtype = "check-back-pic";
    //執行(非同步)
    profile_ref.onSnapshot(querySnapshot => {
        querySnapshot.forEach(function (doc) {
            if (doc.id === profile_id) {
                let img = document.getElementById("check-back-pic");
                img.setAttribute("src", doc.data().background_pic);

                //等照片跑完 且onload為非同步
                img.onload = function () {
                    imgWidth = img.width;
                    imgHeight = img.height;

                    drawCanvas(img, "preview-canvas-back");
                    return;
                }
            }
        });
    });
}

function drawCanvas(img, canvas_id) {
    let canvas = document.getElementById(canvas_id);
    let canvas_side = Math.max(imgWidth, imgHeight);
    canvas.width = canvas_side;
    canvas.height = canvas_side;

    let ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, imgWidth, imgHeight);
}

var record_rotate = 0;
var x = 0;
var y = 0;
function rotateImg() {
    newWidth = record_rotate % 2 == 0 ? imgHeight : imgWidth;
    newHeight = record_rotate % 2 == 0 ? imgWidth : imgHeight;
    let canvas = document.getElementById(canvas_dict[imgtype]);
    let ctx = canvas.getContext("2d");
    ctx.clearRect(x, y, imgWidth, imgHeight);
    ctx.rotate(Math.PI / 2); //弧度制 旋轉坐標軸

    // (0,-imgHeight) 從旋轉原理圖那裡獲得的起始點
    switch (record_rotate) {
        case 0:
            x = 0;
            y = -imgHeight;
            record_rotate += 1;
            break;
        case 1:
            x = -imgWidth;
            y = -imgHeight;
            record_rotate += 1;
            break;
        case 2:
            x = -imgWidth;
            y = 0;
            record_rotate += 1;
            break;
        case 3:
            x = 0;
            y = 0;
            record_rotate = 0;
            break;
    }
    let img = document.getElementById(imgtype);
    ctx.drawImage(img, x, y, imgWidth, imgHeight);    //(x,y,width,height)
}

function uploadImg(type) {
    document.getElementById("uploadFile").click();
}

function confirmImg() {
    let loader = document.getElementById("uploading-img");
    loader.setAttribute("style", "display: block;");
    let cvs = document.getElementById(canvas_dict[imgtype]);
    let cvs2 = document.createElement("canvas");
    cvs2.width = newWidth;
    cvs2.height = newHeight;
    let ctx2 = cvs2.getContext('2d');

    ctx2.drawImage(cvs, 0, 0, newWidth, newHeight, 0, 0, newWidth, newHeight); //裁切圖片

    let data = cvs2.toDataURL("image/png", 1);
    //console.log(data);
    if (imgtype == "check-head-pic") {
        profile_ref.doc(profile_id).update({
            head_pic: data
        }).then(() => {
            loader.setAttribute("style", "display: none;")
            alert("更改成功！");
            location.reload();
        });
    } else if (imgtype == "check-back-pic") {
        profile_ref.doc(profile_id).update({
            background_pic: data
        }).then(() => {
            loader.setAttribute("style", "display: none;")
            alert("更改成功！");
            location.reload();
        });
    }

}

function setting() {
    let settingpage = document.getElementById("setting-page");
    settingpage.setAttribute("style", "width: 65%");
    let setting_controls = document.getElementsByClassName("setting-control");
    setTimeout(() => {
        for (let i = 0; i < setting_controls.length; i++) {
            setting_controls[i].setAttribute("style", "display: block;");
        }
    }, 500);
}

function closeSetting() {
    document.getElementById("setting-page").setAttribute("style", "width: 0");
    let setting_controls = document.getElementsByClassName("setting-control");

    for (let i = 0; i < setting_controls.length; i++) {
        setting_controls[i].setAttribute("style", "display: none;");
    }

}

function doOption(action) {
    let option = document.getElementById(action);
    option.setAttribute("style", "display: block; background-color: white; color: #777070;");
    $('.option-modal').on('hidden.bs.modal', function (e) {
        option.setAttribute("style", "display: block");
    });
}

//sign out --> 清空localstorage 跳轉到signin畫面
function signOut() {
    let loader = document.getElementById("uploading-img");
    loader.setAttribute("style", "display: block;");

    profile_ref.doc(profile_id).update({
        isOnline: false
    }).then(() => {
        localStorage.removeItem("profile_id");
        localStorage.removeItem("friend");
        loader.setAttribute("style", "display:none;");
        let today = new Date();
        profile_ref.doc(profile_id).update({
            last_online_time: today.getNowDateTime(),
        }).then(() => {
            location.href = "./SignIn.html"; //"http://localhost:5500/SignIn.html";
        });
    });
}

function changeNameDiscription() {
    profile_ref.doc(profile_id).get().then(doc => {
        let name_input = document.getElementById("name-input");
        let dis_input = document.getElementById("discription-input");
        let data = doc.data();
        name_input.value = data.name;
        name_input.nextElementSibling.innerHTML = `${data.name.length}/10`;
        dis_input.value = data.discription;
        dis_input.nextElementSibling.innerHTML = `${data.discription.length}/100`;
    });
}

function inputName() {
    let input = document.getElementById("name-input");
    let word_limit = input.nextElementSibling;
    word_limit.innerHTML = `${input.value.length}/10`;
}

function inputDiscription() {
    let input = document.getElementById("discription-input");
    let word_limit = input.nextElementSibling;
    word_limit.innerHTML = `${input.value.length}/100`;
}

function confirmChaneNameDiscription() {
    let newname = document.getElementById("name-input").value;
    let newdis = document.getElementById("discription-input").value;

    let alert = document.getElementById("newalert");
    if (newdis.getAllOcuurenceCount("\n") + 1 >= 7) //最後一行+1
    {
        alert.querySelector("div > div > div > h5").innerText = "行數不得超過7行";
        $("#newalert").modal('toggle');
        return;
    }

    profile_ref.doc(profile_id).update({
        name: newname,
        discription: newdis
    }).then(() => {
        alert.querySelector("div > div > div > h5").innerText = "更改成功!"
        $("#newalert").modal('toggle');
    });
}

function displaySecondIntro(friends) {
    let fri_infos = [];
    let info = document.getElementById("slick-info-1");
    friends.asyncForEach(friend => {
        return new Promise(resolve => {
            profile_ref.doc(friend.friend).get().then(doc => {
                fri_infos.push({ id: doc.id, data: doc.data() });
                info.insertAdjacentHTML('beforeend', `
                <div class="friend-div" id="${doc.id}">
                    <div class="friend-head-pic"></div>
                    <div class="friend-name">${doc.data().name}</div>
                </div>
            `);
                document.querySelector(`#${doc.id} > .friend-head-pic`).setAttribute("style", `background-image: url(${doc.data().head_pic});`);
            });
            resolve();
        });
    }).then(() => {
        document.querySelector("#friend-list-title > i").addEventListener('click', function () {
            ReadySearchFriendList(fri_infos);
        });
    });
}

function ReadySearchFriendList(fri_infos) {
    const wall = document.getElementById("friend-list-wall");
    wall.setAttribute("style", "height: 85%;");
    wall.innerHTML = `
        <i id="cancel-friend-list" class="fri-li-ctr fas fa-times-circle" onclick="cancelFriendList()"></i>
        <input type="text" id="friend-list-input" placeholder="請輸入朋友名稱">
        <div id="friend-list-layout"></div>
    `;

    document.querySelector("#friend-list-input").addEventListener('change', function () {
        serachFriendList(fri_infos);
    });

    var choices = [];
    fri_infos.forEach(info => {
        choices.push(info.data.name);
    })

    $("#friend-list-input").autoComplete({
        minChars: 1,
        source: function (term, suggest) {
            var matches = [];
            for (i = 0; i < choices.length; i++) {
                if (choices[i].includes(term)) {
                    matches.push(choices[i]);
                }
            }
            suggest(matches);
        }
    });
}

function cancelFriendList() {
    const wall = document.getElementById("friend-list-wall");
    wall.innerHTML = '';
    wall.setAttribute("style", "height: 0;");
}

function serachFriendList(fri_infos) {
    const layout = document.getElementById("friend-list-layout");
    layout.innerHTML = '';

    let target = document.querySelector("#friend-list-input").value;
    let target_datas = [];
    fri_infos.forEach(info => {
        if (info.data.name.includes(target)) {
            target_datas.push(info);
            return;
        }
    });
    //console.log(target);

    if (target_datas.length == 0) {
        layout.innerHTML = '';
        return;
    }

    target_datas.forEach(target_data => {
        layout.insertAdjacentHTML('beforeend', `
            <div class="fri-li-div" id="${target_data.id}-li" onclick="clickFriendListSelection(this.id)">
                <div class="fri-li-head-pic"></div>
                <div class="fri-li-name">${target_data.data.name}</div>
            </div>
        `);
        document.querySelector(`#${target_data.id}-li > .fri-li-head-pic`).setAttribute("style", `background-image: url("${target_data.data.head_pic}")`);
    });
}

function clickFriendListSelection(divid) {
    let theDiv = document.getElementById(divid);
    theDiv.setAttribute("style", "background-color: #4a4a4a;");
    delay(() => { theDiv.setAttribute("style", ""); }, 100);
}