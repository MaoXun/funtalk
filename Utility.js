var firebaseConfig = {
    apiKey: "AIzaSyCMHAmFcEHElXFrBNY01TVISRxELNvbKHE",
    authDomain: "chatroomapp-1605b.firebaseapp.com",
    projectId: "chatroomapp-1605b",
    storageBucket: "chatroomapp-1605b.appspot.com",
    messagingSenderId: "636806603367",
    appId: "1:636806603367:web:197403a8501efc8c24aac2",
    measurementId: "G-WD2H91WL3L",
    storageBucket: 'chatroomapp-1605b.appspot.com'
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var db = firebase.firestore();

function focusIcon(id) {
    outActiveIcon();
    setActiveIcon(id)
    setBallPosition(id);
    //console.log(document.getElementById(id).getBoundingClientRect());
}

function gotoTargetPage(id) {
    sessionStorage.setItem("target-focus-icon", id);
    let dict = {
        "chat-icon": "ChatList",
        "profile-icon": "Profile",
        "video-icon": "ChatList"
    };
    location.href = `${dict[id]}.html`;
}

function outActiveIcon() {
    let $navicons = document.getElementsByClassName("nav-icon");
    for (let i = 0; i < $navicons.length; i++) {
        $navicons[i].classList.remove("active-nav-icon");
    }
}

function setActiveIcon(id) {
    document.getElementById(id).classList.add("active-nav-icon");
}

function setBallPosition(id) {
    let x = document.getElementById(id).getBoundingClientRect().x.toFixed(1) - 6;
    let dict = {
        "chat-icon": `display:inline-block; left:${x}px; background: linear-gradient(to left, #867b5e, #7e7256);`,
        "profile-icon": `display:inline-block; left:${x}px; background: linear-gradient(to left, #92876a, #8c8164);`,
        "video-icon": `display:inline-block; left:${x}px; background: linear-gradient(to left, #a09477, #998d71);`,
    };

    document.getElementById("active-ball").setAttribute("style", dict[id]);
    sessionStorage.setItem("original-focus-icon", id);
}

Date.prototype.getNowDateTime = function () {
    //ios android 兼容性問題 要用/
    let currentDateTime = this.getFullYear() + "/" + (this.getMonth() + 1).parseNumRule() + "/" + this.getDate().parseNumRule() + " " + this.getHours().parseNumRule() + ':' + this.getMinutes().parseNumRule() + ':' + this.getSeconds().parseNumRule();
    return currentDateTime;
}

Number.prototype.parseNumRule = function () {
    let num = this < 10 ? "0" + this.toString() : this.toString();
    return num;
}

//Bubble sort
Array.prototype.messageBubbleSortDateTimeAsc = function (param) {
    //bubble sort
    for (let i = 0; i < this.length - 1; i++) {
        for (let j = 0; j < this.length - 1 - i; j++) {
            if (new Date(this[j][param]) - new Date(this[j + 1][param]) > 0) //較靠近現在
            {
                let temp = this[j];
                this[j] = this[j + 1];
                this[j + 1] = temp;
            } else {
                continue;
            }
        }
    }
    return this;
}

Array.prototype.messageBubbleSortDateTimeDesc = function (param) {
    //bubble sort
    for (let i = 0; i < this.length - 1; i++) {
        for (let j = 0; j < this.length - 1 - i; j++) {
            if (new Date(this[j][param]) - new Date(this[j + 1][param]) < 0) //較靠近現在
            {
                let temp = this[j];
                this[j] = this[j + 1];
                this[j + 1] = temp;
            } else {
                continue;
            }
        }
    }
    return this;
}

//Quick sort
Array.prototype.arrayMapQuickSortDateTimeAsc = function (param) {
    //quick sort
    quickSort(this, 0, this.length - 1, param, "asc");
}

Array.prototype.arrayMapQuickSortDateTimeDesc = function (param) {
    //quick sort
    quickSort(this, 0, this.length - 1, param, "desc");
}

function quickSort(array, left, right, param, order) {
    if (left >= right) {
        return;
    }

    let partitionIndex = order == "asc" ? partitionAsc(array, left, right, param) : partitionDesc(array, left, right, param);
    quickSort(array, left, partitionIndex - 1, param, order);
    quickSort(array, partitionIndex + 1, right, param, order);
}

function partitionAsc(array, left, right, param) {
    let pivot = new Date(array[right][param]);
    let leftIndex = left;
    let rightIndex = right - 1;

    while (true) {
        while (leftIndex < right && new Date(array[leftIndex][param]) < pivot) {
            leftIndex++;
        }
        while (rightIndex >= left && new Date(array[rightIndex][param]) > pivot) {
            rightIndex--;
        }
        if (leftIndex > rightIndex) //交錯
        {
            break;
        }
        swap(array, leftIndex, rightIndex);
    }

    //Now leftIndex is the firs element bigger than pivot
    //swap pivot to the right position
    swap(array, leftIndex, right);
    return leftIndex; //return pivot(the middle)
}

function partitionDesc(array, left, right, param) {
    let pivot = new Date(array[right][param]);
    let leftIndex = left;
    let rightIndex = right - 1;

    while (true) {
        while (leftIndex < right && new Date(array[leftIndex][param]) > pivot) {
            leftIndex++;
        }
        while (rightIndex >= left && new Date(array[rightIndex][param]) < pivot) {
            rightIndex--;
        }
        if (leftIndex > rightIndex) //交錯
        {
            break;
        }
        swap(array, leftIndex, rightIndex);
    }

    //Now leftIndex is the firs element bigger than pivot
    //swap pivot to the right position
    swap(array, leftIndex, right);
    return leftIndex; //return pivot(the middle)
}

function swap(array, left, right) {
    let temp = array[left];
    array[left] = array[right];
    array[right] = temp;
}


//非同步版本 forEach
//參數為一個callback函式
Array.prototype.asyncForEach = async function (callback) {
    // this represents our array
    for (let index = 0; index < this.length; index++) {
        // We call the callback for each entry
        await callback(this[index], index, this);  //其餘參數不給會自動被定義為undefined
    }
}

function delay(callback, ms) {
    return new Promise(resolve => {
        setTimeout(() => {
            callback();
            resolve();
        }, ms);
    });
}

//雙指針
String.prototype.getAllOcuurenceCount = function (str) {
    let i = 0, j = 0;
    let count = 0;
    while (i <= this.length - 1) {
        if (!this.substring(j, i).includes(str)) {
            i++;
        } else {
            count++;
            j = i;
        }
    }
    return count;
}