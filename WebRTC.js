//client 端

//使用 WebSocket 的網址向 Server 開啟連結
//WebSocket 是一種網路協定
// http:// ~ ws://
var profile_id = localStorage.getItem("profile_id");
var friend = localStorage.getItem("friend");
var roomId = "";
const userId = localStorage.getItem("profile_id");
var ws = {};
var data = {
    roomId: roomId,
    userId: userId,
    data: {},
    funcName: ""
};
/*
    data={
        roomId: ...,
        userId: ...,
        data: {...},
        funcName: "..."
    }
*/

var localStream = null;
var remoteStream = null;
var config = {
    iceServers: [
        {
            "urls": ["stun:stun.l.google.com:19302",
                "stun:stun1.l.google.com:19302",
                "stun:stun2.l.google.com:19302"]
        }
    ]
};

var pc = new RTCPeerConnection(config);
var iceCandidates = [];
var remoteIceCandidates = [];

var isCaller = false;

const myVideo = document.getElementById("myvideo");
const remoteVideo = document.getElementById("remotevideo");

//狀態
/*
    state
        calling --> A calling B, A's state is calling
        getCalling --> A calling B, B's state is getCalling
        connected --> when B accept A's calling, their state will be connected
 */
$(document).ready(function () {
    let interval = setInterval(() => {
        //console.log("before: "+window.state);
        if (window.state) {
            //console.log("after: "+window.state);
            (() => {
                return new Promise(resolve => {
                    initWindow();
                    resolve();
                });
            })().then(() => {
                clearInterval(interval);
            });
        }
    }, 500);
    setTimeout(() => {
        detectConnection();
    }, 2000);
});

async function detectConnection() {
    await db.collection("Message").doc(profile_id).onSnapshot(async (doc) => {
        if (doc.data().connected == true) //雙向的 所以只要看一個
        {
            await bothConnected();
        }

        //close connection from other
        if (doc.data().beCalled == "" && doc.data().calling == "" && doc.data().callMode == "" && doc.data().connected == false) {
            //會有自己一開始自己關閉的bug 所以用setTimeout
            window.close();
        }
    });
}

async function initWindow() {
    //arrange display by state
    console.log("WebRTC");
    console.log(window.state);

    restraint = window.mode == "audio" ? { audio: true } : (window.mode == "video") ? { audio: true, video: true } : {};
    await init();

    await joinRoom(window.roomid);

    //如果是接收方 連線到socket room 就是成功連線
    if (window.state == "getCalling") {
        await db.collection("Message").doc(profile_id).update({
            connected: true
        });

        await db.collection("Message").doc(friend).update({
            connected: true
        });
    }

    switch (window.state) {
        case 'calling':
            calling();
            break;
    }
}

async function init() {
    console.log("Step1. get user media");
    localStream = await navigator.mediaDevices.getUserMedia(restraint);
    remoteStream = new MediaStream();

    //Push track from localstream and add them with localstream to peer connection
    localStream.getTracks().forEach(track => {
        pc.addTrack(track, localStream);
    });

    //Pull tracks from remote peer and add to local remote stream
    pc.ontrack = e => {
        console.log("<Be add track>");
        e.streams[0].getTracks().forEach(track => {
            remoteStream.addTrack(track);
        })
    }

    myVideo.srcObject = localStream;
    remoteVideo.srcObject = remoteStream;
}

//開啟後執行的動作，指定一個 function 會在連結 WebSocket 後執行
function initWs() {
    //接收 Server 發送的訊息
    ws.onmessage = event => {
        if (event) {
            doAction(JSON.parse(event.data));
        }
    }

    ws.onopen = () => {
        console.log('open connection');
        console.log("room id: " + roomId);
        console.log("user id: " + userId);
        data.data = "First Open! client: " + userId;
        data.funcName = "sendBackToOthers";
        if (ws.readyState === 1) {
            /*
                websocket  ready state
                    CONNECTING = 0
                    OPEN = 1
                    CLOSING = 3
                    CLOSE = 4
            */
            ws.send(JSON.stringify(data));
        }
    }

    //關閉後執行的動作，指定一個 function 會在連結中斷後執行
    ws.onclose = () => {
        console.log('close connection');
    }
}

async function doAction(data) {
    let funcName = data.funcName;
    switch (funcName) {
        case "sendBackToOthers":
            logMessage(data);
            break;
        case "getCall":
            getCall(data)
            break;
        case "getAns":
            getAns(data);
            break;
        case "getRemoteCandidate":
            await getRemoteCandidate(data);
            break;
        default:
            console.log("No function to excute");
    }
}

function logMessage(data) {
    console.log("From Remote: ");
    console.log(data.data);
}

async function getAns(remotedata) {
    console.log("Step4. get remote ans and set remote ans");
    if (!pc.currentRemoteDescription && remotedata?.data) {
        const ansDesc = new RTCSessionDescription(remotedata.data);
        await pc.setRemoteDescription(ansDesc);
    }

    console.log("Step5. send local ice candidate");
    await sendLocalIceCandidate();
}

async function getCall(remotedata) {
    console.log("Step3. get call and set remote offer and create and send answer");

    pc.onicecandidate = e => {
        if (e.candidate) {
            console.log("New answer ICE candidate!");
            iceCandidates.push(e.candidate);
        }
    }

    const remoteOffer = remotedata.data;
    await pc.setRemoteDescription(new RTCSessionDescription(remoteOffer));

    let ansDesc = await pc.createAnswer();
    await pc.setLocalDescription(ansDesc);
    //new ice candidate

    data.funcName = "answerCall";
    data.data = ansDesc;
    console.log(data.userId + "answer");
    ws.send(JSON.stringify(data));
}

async function getRemoteCandidate(remotedata) {
    console.log("Step6. get remote ice candidate and add to peer connection");
    if (remotedata.data) {
        remoteIceCandidates = remotedata.data;
        console.log("add remote active candidate");
        for (let c of remotedata.data) {
            const candidate = new RTCIceCandidate(c);
            await pc.addIceCandidate(candidate);
        }
        if (!isCaller) {
            sendLocalIceCandidate();
        }
    }
}

async function sendLocalIceCandidate() {
    data.funcName = "newIceCandidate";
    data.data = iceCandidates;
    ws.send(JSON.stringify(data));
}

function generateRandomRoomId() {
    //roomId = uuidv4();
    //roomidInput.value = roomId;
    return uuidv4();
}

//have problem
async function joinRoom(existRoom) {
    return new Promise(resolve => {
        console.log("room: " + existRoom);
        roomId = existRoom;
        data.roomId = roomId;
        //ws = new WebSocket(`ws://localhost:3000/${roomId}`);
        ws = new WebSocket(`wss://pure-tor-38549.herokuapp.com/${roomId}`);
        //fix closing problem
        //https://stackoverflow.com/questions/61448531/websocket-connection-is-closing-automatically
        initWs();
        resolve();
    });
}

async function call() {
    console.log("Step2. call remote and create local offer sdp");
    isCaller = true;

    //When set local sdp, ice candidate will immediately generate
    pc.onicecandidate = e => {
        if (e.candidate) {
            console.log("New Offer ICE candidate!");
            iceCandidates.push(e.candidate);
        }
    }

    //create offer
    const offerDesc = await pc.createOffer();
    await pc.setLocalDescription(offerDesc);

    data.funcName = "callRemote";
    data.data = offerDesc;
    console.log(data.userId + " is calling");
    ws.send(JSON.stringify(data));
}

function hangup() {
    //要先掛對方再掛自己
    db.collection("Message").doc(friend).update({
        calling: "",
        beCalled: "",
        callMode: "",
        connected: false
    }).then(() => {
        db.collection("Message").doc(profile_id).update({
            calling: "",
            beCalled: "",
            callMode: "",
            connected: false
        })
    }).then(() => {
        window.close();
    });
}

function calling() {
    let mask = document.getElementById("callingMask");

    db.collection("Profile").doc(friend).get().then(doc => {
        document.getElementById("head").setAttribute("style", `background-image: url("${doc.data().head_pic}")`);
    }).then(() => {
        mask.setAttribute("style", "display: block;");
    });
}

async function bothConnected() {
    document.getElementById("callingMask").setAttribute("style", "display: none;");
    if (window.state == "calling") {
        await call();
    }

    window.state = "connected";
}

function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}
