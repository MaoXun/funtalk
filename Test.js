var firebaseConfig = {
    apiKey: "AIzaSyCMHAmFcEHElXFrBNY01TVISRxELNvbKHE",
    authDomain: "chatroomapp-1605b.firebaseapp.com",
    projectId: "chatroomapp-1605b",
    storageBucket: "chatroomapp-1605b.appspot.com",
    messagingSenderId: "636806603367",
    appId: "1:636806603367:web:197403a8501efc8c24aac2",
    measurementId: "G-WD2H91WL3L"
};

firebase.initializeApp(firebaseConfig);
let db = firebase.firestore();
let profile_ref = db.collection('Profile');
let message_ref = db.collection('Message');

function upload() {

    let base64string = "";

    let files = document.getElementById("uploadFile").files;
    //console.log(files);
    var reader = new FileReader();
    reader.readAsDataURL(files[0]);    // 解析成base64格式
    reader.onload = function () {
        //console.log(this.result);
        base64string = this.result;        // 解析後的資料
        console.log(base64string);
        // profile_ref.doc('vPtFxCZzRXJFMwiPoRUt').update({
        //     head_pic: base64string
        // });
    };
}

function testRegister() {
    //Promise(資料撈取(類似JQuery)，可以用.then.catch方法)寫法
    firebase.auth().createUserWithEmailAndPassword("test2@gmail.com", "test2password")
        .then((userInfo) => {
            profile_ref.doc(userInfo.user.uid).set({
                birth_date: "2021-08-14",
                createTime: "2021-08-14",
                email: "test2@gmail.com",
                friends: ["EdiYVCgphewZtjFdiQwo"],
                head_pic: "",
                name: "testRegister"
            });
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(errorCode + " " + errorMessage);
        });
}

function testSignin() {
    firebase.auth().signInWithEmailAndPassword("test@gmail.com", "test2password")
        .then((userInfo) => {
            console.log(userInfo.user.uid);
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(errorCode + " " + errorMessage);
        });
}

function facebookSignin() {
    var provider = new firebase.auth.FacebookAuthProvider()

    //Popup
    firebase.auth().signInWithPopup(provider)
        .then(function (result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            console.log(user);
            // profile_ref.doc(user.uid).set({
            //     birth_date: "2021-08-14",
            //     createTime: "2021-08-14",
            //     email: "a22908352@gmail.com",
            //     friends: ["5vNl2R6UqQbtwdKdfQG4NVLycJu1"],
            //     head_pic: "",
            //     name: "testFacebookRegister"
            // });

            console.log("success !!");
        }).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;

            console.log(errorCode + " " + errorMessage);
        });
}

function googleSignin() {
    var provider = new firebase.auth.GoogleAuthProvider()

    //Popup
    firebase.auth().signInWithPopup(provider)
        .then(function (result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;

            profile_ref.doc(user.uid).set({
                birth_date: "2021-08-14",
                createTime: "2021-08-14",
                email: "a22908352@gmail.com",
                friends: ["5vNl2R6UqQbtwdKdfQG4NVLycJu1"],
                head_pic: "",
                name: "testGoogleRegister"
            });

            console.log("success !!");
        }).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;

            console.log(errorCode + " " + errorMessage);
        });
}

function testGetMessage() {
    message_ref.doc("04XHpJgRNfgUnZjiVlzXLAxQeWL2").get()
        .then(doc => {
            console.log(doc.data());

        });
}

function changeDateFormat() {
    let mesarray = [];
    let friends = [];

    message_ref.get().then(querySnapshot => {
        querySnapshot.forEach(doc => {
            mesarray.push({ "id": doc.id, "data": doc.data() });
        });
        //console.log(mesarray);

        mesarray.asyncForEach(doc => {
            return new Promise(function (resolve, reject) {
                profile_ref.doc(doc.id).get().then((person) => {
                    friends = person.data().friends;
                    //console.log(friends);
                }).then(() => {
                    friends.asyncForEach(friend => {
                        return new Promise(function (resolve, reject) {
                            message_ref.doc(doc.id).collection(friend.friend).get().then(querySnapshot2 => {
                                querySnapshot2.forEach(doc2 => {
                                    let date = doc2.data().sendTime;
                                    date = date.replaceAll("-", "/");
                                    console.log(date);

                                    message_ref.doc(doc.id).collection(friend.friend).doc(doc2.id).update({
                                        sendTime: date
                                    });
                                });
                                resolve();
                            });
                        });

                    }).then(() => {
                        resolve();
                    });
                })
            });
        }).then(() => {
            console.log("end");
        });
    })

}

function test() {
    console.log("test");
}

Array.prototype.asyncForEach = async function (callback) {
    // this represents our array
    for (let index = 0; index < this.length; index++) {
        // We call the callback for each entry
        await callback(this[index], index, this);  //其餘參數不給會自動被定義為undefined
    }
}

async function testlimit() {
    for (let i = 0; i < 5100; i++) {
        db.collection("test").add({
            test: "test" + i
        });
    }
}