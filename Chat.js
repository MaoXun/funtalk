var profile_id = localStorage.getItem("profile_id");
var friend = localStorage.getItem("friend");
var message_ref;
var profile_ref = db.collection('Profile');
var isFriendActive = false;
let messages;
var rooms;

function init(id) {
    return new Promise(resolve => {
        db.collection('Message').doc(id).get().then((result) => {
            if (!result.exists) {
                db.collection('Message').doc(id).set({
                    active: true,
                    beCalled: "",
                    calling: "",
                    callMode: "",
                    connected: false
                })
            } else {
                db.collection('Message').doc(id).update({
                    beCalled: "",
                    calling: "",
                    callMode: "",
                    connected: false
                })
            }
        })
        resolve();
    })
}

$(document).ready(function () {
    init(profile_id).then(() => {
        return new Promise(resolve => {
            message_ref = db.collection("Message").doc(profile_id);
            init(friend);
            resolve();
        });
    }).then(() => {
        let picloader = document.getElementsByClassName("pic-loader")[0];

        let head_pic = document.getElementById("headpic");
        let name = document.querySelector("#name > span");
        let lastactivetime = document.getElementById("last-active-time");
        let headpic = "";

        profile_ref.onSnapshot(querySnapshot => {
            querySnapshot.forEach(doc => {
                if (doc.id === friend) {
                    head_pic.setAttribute("style", `background-image: url(${doc.data().head_pic})`);
                    headpic = doc.data().head_pic;
                    name.innerText = doc.data().name;
                    lastactivetime.innerText = doc.data().isOnline == false ? getLastOnlineTime(doc.data().last_online_time) : "上線中";

                    return;
                }
            });
            picloader.setAttribute("style", "display: none");
        })

        db.collection('Message').doc(friend).onSnapshot(doc => {
            if (!doc.data().active) {
                db.collection('Message').doc(friend).update({
                    active: true
                }).then(() => {
                    isFriendActive = doc.data().active;
                });
            } else {
                isFriendActive = doc.data().active;
            }
        });

        messages = [];
        message_ref.update({
            active: true,
            beCalled: "",
            calling: "",
            callMode: "",
            connected: false
        }).then(() => {
            message_ref.collection(friend).onSnapshot(querySnapshot => { //即時監聽
                let chatcontent = document.getElementById("chat-content");
                chatcontent.innerHTML = "";
                messages = [];
                querySnapshot.forEach(doc => {
                    messages.push(doc.data());
                    //console.log(doc.data()); //取得
                });
                //firebase可設定排序
                messages.arrayMapQuickSortDateTimeAsc("sendTime");

                //刪訊息4000則
                deleteExcessMessage(messages);
                //console.log(messages);

                messages.forEach((docdata, index) => {
                    if (docdata.sender == profile_id) {
                        chatcontent.insertAdjacentHTML('beforeend', `
                        <div class="message-layout my-layout"><div class="message my-message"><div class="word-block"><span>${docdata.message}</span></div></div></div>
                    `);
                    } else if (docdata.sender == friend) {
                        if (index > 0 && messages[index - 1].sender == friend) {
                            chatcontent.insertAdjacentHTML('beforeend', `
                            <div class="message-layout friend-layout">
                                <div class="message friend-message">
                                    <div class="word-block"><span>${docdata.message}</span></div> 
                                </div>
                            </div>
                        `);
                        } else {
                            chatcontent.insertAdjacentHTML('beforeend', `
                            <div class="message-layout friend-layout">
                                <div class="message friend-message">
                                    <div class="friend-headpic"></div> 
                                    <div class="word-block"><span>${docdata.message}</span></div> 
                                </div>
                            </div>
                        `);
                        }
                    }

                    let friendheadpic = document.getElementsByClassName("friend-headpic");
                    for (let i = 0; i < friendheadpic.length; i++) {
                        friendheadpic[i].style = `background-image: url("${headpic}")`;
                    }
                    let lastmes = document.getElementsByClassName("message-layout");
                    lastmes = lastmes[lastmes.length - 1];
                    lastmes.scrollIntoView();//拉至最下面
                });
            });
        });

        //set socket room
        message_ref.get().then(doc => {
            rooms = doc.data()?.rooms ?? {}; //如果doc.data() && doc.data().rooms 則 doc.data().rooms 否則 {}
            //console.log(rooms);
            if (!rooms[friend]) {
                let roomid = uuidv4();
                rooms[friend] = roomid;
                console.log(rooms);
                message_ref.update({
                    rooms: rooms
                }).then(() => {
                    let friRef = db.collection("Message").doc(friend);
                    friRef.get().then(doc => {
                        let friRooms = doc.data()?.rooms ?? {};
                        friRooms[profile_id] = roomid;
                        friRef.update({
                            rooms: friRooms
                        }).then(() => {
                            //joinRoom(rooms[friend]);
                        });
                    });
                });
            } else {
                //joinRoom(rooms[friend]);
            }
        });
        detectPhonCall();
    });
});

function detectPhonCall() {
    //if beCalled changed, alert request call
    message_ref.onSnapshot(doc => {
        if (doc.data().beCalled != "" && doc.data().connected == false) {
            let getCall = confirm(doc.data().beCalled + " is calling you")
            if (getCall == true) {
                getPhoneCall(doc.data().callMode);
            }
        }
    });
}

function back() {
    message_ref.update({
        active: false
    })
    location.href = "ChatList.html";
}

function sendMessage() {
    const textbox = document.getElementById("textbox");
    const content = textbox.value;
    textbox.value = "";

    let my_message = message_ref.collection(friend).doc();
    let friend_message = db.collection('Message').doc(friend).collection(profile_id).doc();

    let thefriend = profile_ref.doc(friend);
    let friends_array = [];
    let unread = 0;

    let today = new Date();
    let currentDateTime = today.getNowDateTime();

    if (content === "") {
        return;
    } else {
        thefriend.get().then(doc => { //取得朋友資料
            my_message.set({ //更新我的訊息
                message: content,
                sendTime: currentDateTime,
                sender: profile_id
            }).then(() => {
                friend_message.set({ //更新朋友訊息
                    message: content,
                    sendTime: currentDateTime,
                    sender: profile_id
                }).then(() => {
                    //更新frieands資料 --> 取得data --> 先remove再add
                    friends_array = doc.data().friends;
                    friends_array.forEach(function (item, index) {
                        if (item.friend == profile_id) {
                            unread = item.unread_count;
                            if (!isFriendActive) {
                                unread++;
                            }
                            let newdata = {
                                friend: profile_id,
                                unread_count: unread,
                                addDate: item.addDate
                            };

                            //remove
                            thefriend.update({
                                friends: firebase.firestore.FieldValue.arrayRemove(item)
                            }).then(() => {
                                //add
                                thefriend.update({
                                    friends: firebase.firestore.FieldValue.arrayUnion(newdata)
                                }).then(() => {
                                    return;
                                });
                            });
                        }
                    });
                });
            });
        });

    }

}

function getLastOnlineTime(lasttime) {
    let ans = "";

    let today = new Date(new Date().getNowDateTime());
    let diff = (today - new Date(lasttime)) / 1000;

    //1分鐘前 --> Online
    //1分鐘 ~ 59分鐘 --> 分鐘
    //1小時~23小時 --> 小時
    //1天 ~ 1天又23小時 -->一天前
    //2天 --> 不顯示

    if (diff < 60) {
        ans = "上線中";
    } else if (1 * 60 <= diff && diff < 60 * 60) {
        diff = diff / 60; //分
        ans = Math.floor(diff).toString() + "分鐘前上線";
    } else if (1 * 60 * 60 <= diff && diff < 24 * 60 * 60) {
        diff = diff / (60 * 60); //時
        ans = Math.floor(diff).toString() + "小時前上線";
    } else if (24 * 60 * 60 <= diff && diff < 48 * 60 * 60) {
        ans = "1天前上線";
    } else {
        ans = "";
    }

    return ans;
}

function openFileUploader(input_id) {
    let input = document.getElementById(input_id);
    input.click();
}

function uploadFile(input_id) {
    let input = document.getElementById(input_id);

    // 取得檔案資訊
    const file = input.files[0];
    const path = file.name;

    // 取得 storage 對應的位置
    const storageReference = firebase.storage().ref(path);

    // .put() 方法把東西丟到該位置裡
    const task = storageReference.put(file);
}

function deleteExcessMessage(messages) {
    let docid;
    if (messages.length >= 4000) {
        oldestMessage = messages[0];
        message_ref.collection(friend).get().then(ref => {
            ref.forEach(doc => {
                if (JSON.stringify(doc.data()) == JSON.stringify(oldestMessage)) {
                    docid = doc.id;
                }
            });
        });

        message_ref.collection(friend).doc(docid).delete();
    }
}

async function startPhoneCall() {
    // let callPage = window.open("http://localhost:5500/WebRTC.html");
    let callPage = window.open("./WebRTC.html");
    callPage.roomid = rooms[friend];
    let friendName = "";
    await db.collection("Profile").doc(friend).get().then((doc) => {
        friendName = doc.data().name;
        callPage.document.title = "與" + friendName + "的通話";

        //if calling, change attribute on firebase to let remote know he/she is being calling
        //beCalled: "" or "{callerid}", calling: "" or {recieverid}
        message_ref.update({
            calling: friend,
            callMode: "audio",
        }).then(() => {
            db.collection("Message").doc(friend).update({
                beCalled: profile_id,
                callMode: "audio",
            }).then(() => {
                //displayed
                callPage.mode = "audio";
                callPage.state = "calling";
            });
        });

        //once the beCalled(belongs to me, so others won't change) changed, alert, if accept, add in socket room
        //doc.onSnapshot... after ready function

    });
}

async function startVideoCall() {
    let callPage = window.open("_blank");
    callPage.location = "./WebRTC.html";
    callPage.roomid = rooms[friend];
    let friendName = "";
    await db.collection("Profile").doc(friend).get().then((doc) => {
        friendName = doc.data().name;
        callPage.document.title = "與" + friendName + "的通話";

        //if calling, change attribute on firebase to let remote know he/she is being calling
        //beCalled: "" or "{callerid}", calling: "" or {recieverid}
        message_ref.update({
            calling: friend,
            callMode: "video",
        }).then(() => {
            db.collection("Message").doc(friend).update({
                beCalled: profile_id,
                callMode: "video",
            }).then(() => {
                //displayed
                callPage.mode = "video";
                callPage.state = "calling";
            });
        });
    });
}

function getPhoneCall(mode) {
    //瀏覽器有pop-up block problem
    //因此改成先開一個視窗 再重新導向
    let callPage = window.open("_blank");
    callPage.location = "./WebRTC.html";

    callPage.roomid = rooms[friend];
    let friendName = "";
    db.collection("Profile").doc(friend).get().then((doc) => {
        friendName = doc.data().name;
        callPage.document.title = "與" + friendName + "的通話";

        //need firebase to determine
        callPage.mode = mode;
        callPage.state = "getCalling";
    })
}

function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}