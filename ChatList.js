var profile_id = localStorage.getItem("profile_id");

$('document').ready(function () {
    //document.getElementById("chat-icon").classList.add("active-nav-icon");
    if (sessionStorage.getItem("original-focus-icon")) {
        focusIcon(sessionStorage.getItem("original-focus-icon"));
    }
    focusIcon("chat-icon");
    //必須加在這邊 不可以在html註冊 否則removeEventListener無法作用在
    document.querySelector("#notification > i").addEventListener('click', openNotification);

    searchFriends();
    displayNotiCount();
});

function chat(id) {
    document.getElementById(id).setAttribute("style", "background-color: #e0a7a7;");
    delay(() => { document.getElementById(id).setAttribute("style", "background: hsla(0,0%,100%,.3);"); }, 100);

    document.getElementById("waiting").setAttribute("style", "display: block;");

    localStorage.setItem("friend", id);

    let ref = db.collection('Profile').doc(profile_id);
    ref.get().then(doc => {
        doc.data().friends.forEach(function (item, index) {
            if (item.friend == id) {
                ref.update({
                    friends: firebase.firestore.FieldValue.arrayRemove(item)
                }).then(() => {
                    let newdata = {
                        friend: id,
                        unread_count: 0,
                        addDate: item.addDate
                    }
                    ref.update({
                        friends: firebase.firestore.FieldValue.arrayUnion(newdata)
                    }).then(() => {
                        document.getElementById("waiting").setAttribute("style", "display: none;");
                        location.href = "Chat.html";
                    });
                });
            }
        });
    });

}

function searchFriends() {
    let profile_ref = db.collection('Profile');
    let message_ref = db.collection('Message').doc(profile_id);

    let friends = [];
    let messages = [];

    //執行
    profile_ref.get().then(querySnapshot => {
        document.getElementById("glass").innerHTML = "";
        friends = [];
        messages = [];
        querySnapshot.forEach(doc => {
            //let friends_id = doc.data().friends;
            if (doc.id === profile_id) {
                let friends_info = doc.data().friends;
                for (let i = 0; i < friends_info.length; i++) {
                    querySnapshot.forEach(doc => {
                        if (doc.id == friends_info[i]["friend"]) {
                            friends.push({
                                "id": friends_info[i]["friend"],
                                "data": doc.data(),
                                "last_message": "",  //應顯示兩人message中最晚的訊息
                                "last_message_time": "",
                                "unread_count": friends_info[i]["unread_count"],
                                "addDate": friends_info[i].addDate
                            });
                        }
                    });
                }

            }
        });

        //遇到 async，別用 forEach，foreach會先執行，自定義一個async foreach
        friends.asyncForEach((item, index) => {
            return new Promise(function (resolve, reject) {
                message_ref.collection(item["id"]).get().then(docs => {
                    messages = [];
                    docs.forEach(doc => {
                        messages.push(doc.data());
                    });
                    messages.arrayMapQuickSortDateTimeAsc("sendTime");

                    friends[index].last_message = messages.length == 0 ? "" : messages[messages.length - 1].message;
                    friends[index].last_message_time = messages.length == 0 ? item.addDate : messages[messages.length - 1].sendTime;

                    resolve(); //execute then()
                });
            });
        }).then(() => {
            friends.arrayMapQuickSortDateTimeDesc("last_message_time");
            friends.forEach(friend => {
                document.getElementById("glass").insertAdjacentHTML('beforeend', `
                            <div class="chater" id="${friend["id"]}" onclick="chat(this.id)">
                                <div style="padding: 1em;">
                                    <div class="brief-header" style="background-image: url(${friend["data"].head_pic});"></div>
                                </div>
                                <div class="name-message-group">
                                    <div class="brief-name"><span>${friend["data"].name}</span></div>
                                    <div class="brief-message"><span>${friend["last_message"]}</span></div>
                                </div>
                                <div class="time-nonread-group">
                                    <div class="brief-time"><span>${parseShowTime(friend["last_message_time"])}</span></div>
                                    <div class="brief-nonread"><span>${friend["unread_count"]}</span></div>
                                </div>
                            </div>
                `);
            });

            const unread = document.querySelectorAll(".brief-nonread > span");
            for (let i = 0; i < unread.length; i++) {
                if (unread[i].innerText == 0) {
                    unread[i].setAttribute("style", "display: none;");
                    unread[i].parentElement.setAttribute("style", "background-color: transparent;");
                }
            }

            let loader = document.getElementsByClassName("loader")[0];
            loader.setAttribute("style", "display: none");
        });
    }); //async func 回傳 promise 可以調用then()
}

function parseShowTime(rawtime) {
    //console.log(rawtime);     
    if (rawtime == "") {
        return "";
    }

    var day_list = ['日', '一', '二', '三', '四', '五', '六'];

    let show = "";

    let today = new Date();
    today = new Date(today.getNowDateTime().substring(0, 10));

    let theDate = new Date(rawtime.substring(0, 10));

    let diffOfDay = (today - theDate) / (24 * 60 * 60 * 1000);

    if (diffOfDay < 1) // this day
    {
        //顯示時間
        let dd = Number(rawtime.substring(11, 13)) > 12 ? "下午" : "上午"
        let hour = Number(rawtime.substring(11, 13)) > 12 ? (Number(rawtime.substring(11, 13)) - 12).parseNumRule() : (Number(rawtime.substring(11, 13))).parseNumRule();
        let min = rawtime.substring(14, 16);
        show = dd + hour + ":" + min;
    } else if (1 <= diffOfDay && diffOfDay < 7) //within one week
    {
        //顯示星期
        show = "星期" + day_list[theDate.getDay()];
    } else //>=7 
    {
        show = Number(rawtime.substring(5, 7)).toString() + "/" + Number(rawtime.substring(8, 10)).toString();
    }
    return show;
}

var allpeople = [];
function readySearch() {
    document.getElementById("search-result").innerHTML = '';
    allpeople = [];

    db.collection('Profile').get().then((querySnapshot) => {
        querySnapshot.forEach(doc => {
            allpeople.push({ "id": doc.id, "data": doc.data() });
        });
    });

    let searchwall = document.getElementById("search-wall");
    searchwall.setAttribute("style", "display: block;");

    let bell = document.querySelector("#notification > i");
    bell.classList.add("fa-times");
    bell.classList.remove("fa-bell");
    bell.setAttribute("style", "margin-left: 20px;");

    let noti_count = document.getElementById("noti-count");
    noti_count.setAttribute("style", "display: none");

    bell.addEventListener('click', cancelSearch);
    bell.removeEventListener('click', openNotification);

    document.querySelector("#searchbar > i").setAttribute("style", "display: none");

}

function cancelSearch() {
    let searchwall = document.getElementById("search-wall");
    searchwall.setAttribute("style", "display: none;");

    let bell = document.querySelector("#notification > i");
    bell.classList.remove("fa-times");
    bell.classList.add("fa-bell");
    bell.setAttribute("style", "margin-left: 0;");

    let noti_count = document.getElementById("noti-count");
    noti_count.setAttribute("style", "display: inline-block;");

    document.getElementsByClassName("search-loader")[0].setAttribute("style", "display: none;");
    document.querySelector("#searchbar > i").setAttribute("style", "display: block");
    bell.classList.remove("fa-times");
    document.querySelector("#searchbar > input").value = "";
    bell.addEventListener('click', openNotification);
    bell.removeEventListener('click', cancelSearch);
}

function search() {
    document.getElementById("search-result").innerHTML = '';

    const loader = document.getElementsByClassName("search-loader")[0]
    loader.setAttribute("style", "display: block;");
    //search
    const textbox = document.querySelector("#searchbar > input");

    if (textbox.value == "") {
        return;
    }
    allpeople.forEach((person, index) => {
        if (person.data.name.includes(textbox.value)) {
            //console.log(person.id);               
            document.getElementById("search-result").insertAdjacentHTML('beforeend', `
                <div class="search-person" id="${person.id}" onclick="doSearchAction(this.id);">
                    <div class="search-head-pic"></div>
                    <div class="search-name"><span>${person.data.name}</span></div>
                </div>
            `);

            document.querySelector(`#${person.id} > .search-head-pic`).setAttribute("style", `background-image: url("${person.data.head_pic}");`);
            loader.setAttribute("style", "display: none;");
        } else if (index == allpeople.length - 1 && document.getElementsByClassName("search-person").length == 0) {
            loader.setAttribute("style", "display: none;");
            return;
        }
    });
}

function doSearchAction(id) {
    const selection = document.getElementById(id);
    selection.setAttribute("style", "background-color: #585858;");
    delay(() => { selection.setAttribute("style", "background-color: transparent;"); }, 100);
    goToPersonPage(id);
}

var status_info = {};
var changeHandler;
var popHandler
function goToPersonPage(id) {
    const loader = document.getElementsByClassName("search-loader")[0];
    loader.setAttribute("style", "display: block;");

    let person;
    let me;
    let search_person_page = document.getElementById("search-person-page");

    db.collection('Profile').doc(id).get().then((doc) => {
        db.collection('Profile').doc(profile_id).get().then(doc => {
            return new Promise(resolve => {
                me = { "id": doc.id, "data": doc.data() };
                resolve();
            });
        }).then(() => {
            search_person_page.innerHTML = "";
            person = { "id": id, "data": doc.data() };
            //console.log(person);

            /*
                穩定狀態(未是朋友、已是朋友) --> 暗色 #3a3b3c; --> 未是朋友(加好友)fa-user-plus 已是朋友fa-user-friends
                不穩定狀態(寄送請求中、尚未確認請求中) --> 藍色 #2d88ff --> 寄送請求中fa-user-tag 尚未確認中fa-user-check
            */

            getFriendStatus(id, person).then(() => {
                //console.log(status_info);
                search_person_page.innerHTML = `
                        <div id="background-pic">
                                    
                        </div>
                        <div id="head-group">
                            <div id="head-pic">
                                    
                            </div> 
                            <div id="friend-status" style="background-color: ${status_info.color};" data-toggle="modal"
                            data-target="#isChangeFriendRelationship">
                                <i class="fas ${status_info.class}"></i>
                                <span>${status_info.status}</span>
                            </div> 
                        </div>                    
                        <div id="discription">
                            <div id="introcard">
                                <div class="slider single-item" style="height: 85%;">
                                    <div id="info-1">
                                        <div id="big-name"></div>
                                        <div id="text-content"></div>
                                    </div>
                                    <div id="info-2">
                                        <div id="slick-info-1">
                                            <div id="friend-list-title">好友列表<i class="fas fa-search" style="margin-left: 8px;"></i></div>
                                        </div>
                                    </div>
                                    <div id="info-3">
                                        <h3>代表影片</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <i id="back-to-search" class="fas fa-arrow-left" onclick="backToSearch()"></i>
                    `;

                document.getElementById("head-pic").setAttribute("style", `background-image: url("${person.data.head_pic}")`);
                document.getElementById("background-pic").setAttribute("style", `background-image: url("${person.data.background_pic}")`);
                document.getElementById("big-name").innerText = person.data.name;
                document.getElementById("text-content").innerText = person.data.discription;
                displaySecondIntro(person.data.friends);

                $('.single-item').slick({
                    dots: true
                });

                let sliderHeight = document.getElementsByClassName("slick-list")[0].offsetHeight;
                document.getElementById("slick-info-1").setAttribute("style", `height: ${sliderHeight}px;`);

                setTimeout(() => {
                    document.querySelector("#introcard > div > ul > li > button").click();
                }, 500);

                popHandler = function () {
                    popConfirmWindow(status_info.status, person);
                }
                changeHandler = function () {
                    changeFriendRelationship(status_info, me, person);
                };

                document.getElementById("friend-status").addEventListener('click', popHandler);
                document.getElementById("change-confirm").addEventListener('click', changeHandler);

                if (me.id == person.id) {
                    document.getElementById("friend-status").setAttribute("style", "display: none;");
                }
                search_person_page.setAttribute("style", "width: 100%");
                loader.setAttribute("style", "display: none;");
            });

        });
    });
}

function backToSearch() {
    let search_person_page = document.getElementById("search-person-page");
    search_person_page.setAttribute("style", "width: 0;");
    search_person_page.innerHTML = '';

    let noti_count = document.getElementById("noti-count");
    noti_count.setAttribute("style", "display: none");
}

function getFriendStatus(id, person) {
    return new Promise(resolve => {
        let status_class = "";
        let color = "";
        let status = "";
        let mydata = {};

        db.collection('Profile').doc(profile_id).get().then(doc => {
            mydata = doc.data();
            person.data.friends.forEach(friend => {
                if (friend.friend == profile_id) //互為朋友
                {
                    status_class = "fa-user-friends";
                    color = "#3a3b3c";
                    status = "朋友";
                }
            });
            if (status_class == "") //未是朋友
            {
                person.data.notifications.forEach(notification => {
                    if (notification.person == profile_id && notification.content == (mydata.name + "想要加你為好友")) {
                        status_class = "fa-user-tag";
                        color = "#2d88ff";
                        status = "等待回覆";
                    }
                });
                if (status_class == "") //未是朋友 且 未送請求
                {
                    mydata.notifications.forEach(notification => {
                        if (notification.person == id && notification.content == (person.data.name + "想要加你為好友")) {
                            status_class = "fa-user-check"
                            color = "#2d88ff";
                            status = "回應請求";
                        }
                    });
                    if (status_class == "") //未是朋友 且 未送請求 且未被送請求
                    {
                        status_class = "fa-user-plus";
                        color = "#3a3b3c";
                        status = "加好友";
                    }

                }
            }
            status_info = { "class": status_class, "color": color, "status": status };
            resolve();
        });
    });

    //return status_info; //在最外層function在return 
}

function changeFriendRelationship(current_status, me, person) {
    let myref = db.collection("Profile").doc(profile_id);
    let personref = db.collection("Profile").doc(person.id);

    const change_dict = {
        "朋友": "加好友", //取消好友 //remove from friends
        "加好友": "等待回覆", //加入好友 //add in notifications
        "等待回覆": "加好友", //取消寄送 //remove from notifications
        "回應請求": "朋友" //確認好友 //remove from notifications and add in friends
    };
    const class_dict = {
        "朋友": "fa-user-friends",
        "加好友": "fa-user-plus",
        "等待回覆": "fa-user-tag",
        "回應請求": "fa-user-check"
    };

    //改狀態 改html
    let btn_group = document.getElementById("friend-status");
    let change_color = (change_dict[current_status.status] == "朋友" || change_dict[current_status.status] == "加好友") ? "#3a3b3c" : "#2d88ff";

    //改html
    status_info = { "class": class_dict[change_dict[current_status.status]], "color": change_color, "status": change_dict[current_status.status] }

    //因為有async function 所以不用switch
    if (current_status.status == "朋友") { //remove from friends
        removeFromFriendsMutually(person.id);
    } else if (current_status.status == "加好友") { //add in notifications
        let tempNotification = { addTime: new Date().getNowDateTime(), content: me.data.name + "想要加你為好友", person: profile_id, type: "friend-type" };
        addInNotifications(person.id, tempNotification);
    } else if (current_status.status == "等待回覆") { //remove from 'his/her' notifications
        let theNotification = {};
        personref.get().then(doc => {
            doc.data().notifications.forEach(notification => {
                if (notification.person == me.id && notification.content == (me.data.name + "想要加你為好友")) {
                    theNotification = notification;
                }
            });
            removeFromNotifications(person.id, theNotification);
        });

    } else if (current_status.status == "回應請求") { //remove from notifications and add in friends
        let theNotification = {};
        myref.get().then(doc => {
            let tempNotification = { addTime: new Date().getNowDateTime(), content: me.data.name + "接受了你的交友邀請", person: profile_id, type: "friend-type" };
            doc.data().notifications.forEach(notification => {
                if (notification.person == person.id && notification.content == (person.data.name + "想要加你為好友")) {
                    theNotification = notification;
                }
            });
            removeFromNotifications(profile_id, theNotification);
            addInNotifications(person.id, tempNotification);
            addInFriendsMutually(person.id);
        });
    }

    // <div id="friend-status" style="background-color: ${status_info.color};">
    //    <i class="fas ${status_info.class}"></i>
    //    <span>${status_info.status}</span>
    // </div>

    //更新
    //先移除才能新增 否則共存 只會call到第一個function
    document.getElementById("change-confirm").removeEventListener('click', changeHandler);
    myref.get().then(doc => {
        return new Promise(resolve => {
            me = { "id": doc.id, "data": doc.data() };
            resolve();
        });
    }).then(() => {
        personref.get().then(doc => {
            person = { "id": doc.id, "data": doc.data() };
            //change html
            btn_group.setAttribute("style", `background-color: ${status_info.color};`);
            btn_group.querySelector("i").classList.remove(current_status.class);
            btn_group.querySelector("i").classList.add(status_info.class);
            btn_group.querySelector("span").innerText = status_info.status;

            changeHandler = function () {
                changeFriendRelationship(status_info, me, person);
            };

            document.getElementById("change-confirm").addEventListener('click', changeHandler);
        });
    });
}

function addInFriendsMutually(id) { //雙向
    let myref = db.collection("Profile").doc(profile_id);
    let themanref = db.collection("Profile").doc(id);

    myref.update({
        friends: firebase.firestore.FieldValue.arrayUnion({
            friend: id,
            unread_count: 0,
            addDate: new Date().getNowDateTime()
        })
    }).then(() => {
        themanref.update({
            friends: firebase.firestore.FieldValue.arrayUnion({
                friend: profile_id,
                unread_count: 0,
                addDate: new Date().getNowDateTime()
            })
        });
    });
}

function removeFromFriendsMutually(id) { //雙向
    let myref = db.collection("Profile").doc(profile_id);
    let themanref = db.collection("Profile").doc(id);

    let me, person;
    //unknow the unread_count
    myref.get().then(doc => {
        return new Promise(resolve => {
            doc.data().friends.forEach(friend => {
                if (friend.friend == id) {
                    person = friend;
                    return;
                }
            });
            resolve();
        });
    }).then(() => {
        return new Promise(resolve => {
            themanref.get().then(doc => {
                doc.data().friends.forEach(friend => {
                    if (friend.friend == profile_id) {
                        me = friend;
                        return;
                    }
                });
            });
            resolve();
        });
    }).then(() => {
        myref.update({
            friends: firebase.firestore.FieldValue.arrayRemove(person)
        }).then(() => {
            themanref.update({
                friends: firebase.firestore.FieldValue.arrayRemove(me)
            });
        });
    });
}

function addInNotifications(id, notification) { //對方要收到確認通知
    db.collection("Profile").doc(id).update({
        notifications: firebase.firestore.FieldValue.arrayUnion(notification)
    });
}

function removeFromNotifications(id, notification) { //對方要收到拒絕通知
    db.collection("Profile").doc(id).update({
        notifications: firebase.firestore.FieldValue.arrayRemove(notification)
    });
}

var rejectFriendRequestHandler;
function popConfirmWindow(nowStatus, person) {
    rejectFriendRequestHandler = function () {
        db.collection("Profile").doc(profile_id).get().then(doc => {
            doc.data().notifications.forEach(notification => {
                if (notification.person == person.id && notification.content == (person.data.name + "想要加你為好友")) {
                    removeFromNotifications(profile_id, notification);
                    status_info = { "class": "fa-user-plus", "color": "#3a3b3c", "status": "加好友" }
                    let btn_group = document.getElementById("friend-status");
                    btn_group.setAttribute("style", "background-color: #3a3b3c;");
                    btn_group.querySelector("i").classList.remove("fa-user-check");
                    btn_group.querySelector("i").classList.add("fa-user-plus");
                    btn_group.querySelector("span").innerText = "加好友";
                    return;
                }
            });
        });
    }
    let reject = document.getElementById("close-or-delete");
    reject.removeEventListener('click', rejectFriendRequestHandler);
    reject.innerText = "關閉";
    const content = document.querySelector("#isChangeFriendRelationship > div > div > div > h5");
    let res = "是否";

    switch (nowStatus) {
        case "朋友":
            res += "取消好友?";
            break;
        case "加好友":
            res += "送出交友邀請?";
            break;
        case "等待回覆":
            res += "取消交友邀請?";
            break;
        case "回應請求":
            res += "拒絕或接受邀請";
            reject.innerText = "拒絕";
            reject.addEventListener('click', rejectFriendRequestHandler);
            break;
    }

    content.innerText = res;
}

var notificationsArray = []
function displayNotiCount() {
    db.collection("Profile").doc(profile_id).onSnapshot(doc => {
        notificationsArray = [];
        let noti_count = document.getElementById("noti-count");

        notificationsArray = doc.data().notifications;
        notificationsArray.arrayMapQuickSortDateTimeDesc("addTime");
        if (notificationsArray.length > 0) {
            noti_count.setAttribute("style", "display: inline-block");
            noti_count.innerText = notificationsArray.length;
        }
    });
}

function openNotification() {
    const noti_wall = document.getElementById("notifications-wall");
    noti_wall.setAttribute("style", "display: block;");

    let noti_content = document.getElementById("notifications-content");
    noti_content.innerHTML = '<i class="far fa-times-circle" id="close-noti" onclick="closeNotification()"></i>';
    //文字超過多少(20個字) 要變成...
    let people = {};
    db.collection("Profile").get().then(querySnapshot => {
        return new Promise(resolve => {
            querySnapshot.forEach(doc => {
                people[doc.id] = doc.data().head_pic;
            });
            resolve();
        });
    }).then(() => {
        notificationsArray.forEach((noti, index) => {
            let parse_content = noti.content.length > 27 ? noti.content.substring(0, 27) + "..." : noti.content;
            noti_content.insertAdjacentHTML('beforeend', `
                <div id="${noti.person}-${index}" class="each-noti">
                    <div class="noti-head-pic"></div>
                    <div class="noti-content">
                        <div class="noti-text">${parse_content}</div>
                        <div class="noti-time">${noti.addTime}</div>
                    </div>
                    <i class="fas fa-times delete-noti"></i>
                </div>
            `)
            document.querySelector(`#${noti.person}-${index} > .noti-head-pic`).setAttribute("style", `background-image: url('${people[noti.person]}');`);
            document.querySelector(`#${noti.person}-${index} > i`).addEventListener('click', function () {
                deleteNotification(`${noti.person}-${index}`, noti);
            });
            document.querySelector(`#${noti.person}-${index}`).addEventListener('click', function () {
                doNotiAction(`${noti.person}-${index}`, noti);
            });
        });

        delay(() => { noti_content.style.width = "90%"; }, 300).then(() => {
            delay(() => { noti_content.style.height = "80%"; }, 900);
        });
    });
}

function closeNotification() {
    const noti_wall = document.getElementById("notifications-wall");
    noti_wall.setAttribute("style", "display: none;");

    const noti_content = document.getElementById("notifications-content");
    noti_content.style.width = "0";
    noti_content.style.height = "1%";
}

function deleteNotification(notidivid, thenoti) {
    db.collection("Profile").doc(profile_id).update({
        notifications: firebase.firestore.FieldValue.arrayRemove(thenoti)
    });
    let ele = document.getElementById(notidivid);
    ele.parentElement.removeChild(ele);
}

function doNotiAction(divid, noti) {
    const notidiv = document.getElementById(divid);
    notidiv.setAttribute("style", "background-color: #b7b7b7;");
    delay(() => { notidiv.setAttribute("style", ""); }, 100);

    switch (noti.type) {
        case "friend-type":
            goToPersonPage(noti.person);
            break;
        default:
            return;
    }
}

function displaySecondIntro(friends) {
    let fri_infos = [];
    let info = document.getElementById("slick-info-1");
    friends.asyncForEach(friend => {
        return new Promise(resolve => {
            db.collection("Profile").doc(friend.friend).get().then(doc => {
                fri_infos.push({ id: doc.id, data: doc.data() });
                info.insertAdjacentHTML('beforeend', `
                <div class="friend-div" id="${doc.id}">
                    <div class="friend-head-pic"></div>
                    <div class="friend-name">${doc.data().name}</div>
                </div>
            `);
                document.querySelector(`#${doc.id} > .friend-head-pic`).setAttribute("style", `background-image: url(${doc.data().head_pic});`);
            });
            resolve();
        });
    }).then(() => {
        document.querySelector("#friend-list-title > i").addEventListener('click', function () {
            ReadySearchFriendList(fri_infos);
        });
    });
}

function ReadySearchFriendList(fri_infos) {
    const wall = document.getElementById("friend-list-wall");
    wall.setAttribute("style", "height: 85%;");
    wall.innerHTML = `
        <i id="cancel-friend-list" class="fri-li-ctr fas fa-times-circle" onclick="cancelFriendList()"></i>
        <input type="text" id="friend-list-input" placeholder="請輸入朋友名稱">
        <div id="friend-list-layout"></div>
    `;

    document.querySelector("#friend-list-input").addEventListener('change', function () {
        serachFriendList(fri_infos);
    });

    var choices = [];
    fri_infos.forEach(info => {
        choices.push(info.data.name);
    })

    $("#friend-list-input").autoComplete({
        minChars: 1,
        source: function (term, suggest) {
            var matches = [];
            for (i = 0; i < choices.length; i++) {
                if (choices[i].includes(term)) {
                    matches.push(choices[i]);
                }
            }
            suggest(matches);
        }
    });
}

function cancelFriendList() {
    const wall = document.getElementById("friend-list-wall");
    wall.innerHTML = '';
    wall.setAttribute("style", "height: 0;");
}

function serachFriendList(fri_infos) {
    const layout = document.getElementById("friend-list-layout");
    layout.innerHTML = '';

    let target = document.querySelector("#friend-list-input").value;
    let target_datas = [];
    fri_infos.forEach(info => {
        if (info.data.name.includes(target)) {
            target_datas.push(info);
            return;
        }
    });
    //console.log(target);

    if (target_datas.length == 0) {
        layout.innerHTML = '';
        return;
    }

    target_datas.forEach(target_data => {
        layout.insertAdjacentHTML('beforeend', `
            <div class="fri-li-div" id="${target_data.id}-li" onclick="clickFriendListSelection(this.id)">
                <div class="fri-li-head-pic"></div>
                <div class="fri-li-name">${target_data.data.name}</div>
            </div>
        `);
        document.querySelector(`#${target_data.id}-li > .fri-li-head-pic`).setAttribute("style", `background-image: url("${target_data.data.head_pic}")`);
    });
}

function clickFriendListSelection(divid) {
    let theDiv = document.getElementById(divid);
    theDiv.setAttribute("style", "background-color: #4a4a4a;");
    delay(() => { theDiv.setAttribute("style", ""); }, 100);
}