let today = new Date();
let month = Number(today.getMonth()) + 1 //預設從0開始
let date = today.getFullYear() + "-" + (month < 10 ? "0" + month.toString() : month.toString()) + "-" + today.getDate();
let defaultuserpic = "";
var message_ref = db.collection('Message');

db.collection('Profile').doc("szg8DhlVYTDrIPEKFmJJ").get().then((doc) => {
    defaultuserpic = doc.data().head_pic
})

$(document).ready(function () {
    let inputs = document.querySelectorAll('.info-group > div > input');
    let icons = document.getElementsByClassName('icon-div');
    let logtags = document.getElementsByClassName('log-tag');
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].setAttribute('style', 'width: 100%;');
        icons[i].setAttribute('style', 'transform: scale(1)');
    }
    for (let i = 0; i < logtags.length; i++) {
        logtags[i].setAttribute('style', 'transform: scale(1)');
    }
});

function openRegister() {
    const email = document.getElementById("InputEmail");
    const password = document.getElementById("InputPassword");
    const birth = document.getElementById("InputBirth");
    const name = document.getElementById("InputName");
    const regitems = document.getElementsByClassName("reg-item");
    const labels = document.getElementsByClassName("invalid-label");

    for (let i = 0; i < regitems.length; i++) {
        regitems[i].classList.remove("invalid-border");
        labels[i].setAttribute('style', 'display: none');
    }

    email.value = "";
    password.value = "";
    birth.value = "";
    name.value = "";
}

function register() {
    let loader = document.getElementById("logining");
    loader.setAttribute("style", "display: block");

    const regitems = document.getElementsByClassName("reg-item");
    const labels = document.getElementsByClassName("invalid-label");
    for (let i = 0; i < regitems.length; i++) {
        if (regitems[i].value === "") {
            regitems[i].classList.add("invalid-border");
            labels[i].setAttribute('style', 'display: inline-block');
            if (i == regitems.length - 1) {
                return;
            }
        }
    }

    const email = document.getElementById("InputEmail");
    const password = document.getElementById("InputPassword");
    const birth = document.getElementById("InputBirth");
    const name = document.getElementById("InputName");

    //Promise(資料撈取(類似JQuery)，可以用.then.catch方法)寫法
    firebase.auth().createUserWithEmailAndPassword(email.value, password.value)
        .then((userInfo) => {
            let ref = db.collection('Profile').doc(userInfo.user.uid);
            let message_doc = message_ref.doc(userInfo.user.uid);

            ref.set({
                birth_date: birth.value,
                createTime: date,
                email: email.value,
                friends: [],
                head_pic: defaultuserpic,
                background_pic: "",
                isOnline: false,
                last_online_time: "",
                notifications: [],
                name: name.value,
                isOnline: false,
                background_pic: "",
                discription: ""
            }).then(() => {
                message_doc.set({
                    active: false
                }).then(() => {
                    loader.setAttribute("style", "display: none");
                    alert("註冊成功！");
                    location.reload();
                });
            });
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            //console.log(errorCode + " " + errorMessage);
            let dict = {
                "auth/email-already-in-use": "帳號已存在！",
                "auth/invalid-email": "email不符合規定！",
                "auth/weak-password": "密碼至少6碼以上！"
            };

            alert(dict[errorCode]);
        });

}

var record_see = true;
function seePassword() {
    //<i class="fas fa-eye"></i>
    //<i class="fas fa-eye-slash"></i>
    const div = document.getElementsByClassName("eye")[0];
    const password = document.getElementById("password");
    div.innerHTML = '';
    if (record_see) {
        div.insertAdjacentHTML('beforeend', `
            <i class="fas fa-eye-slash" onclick="seePassword()"></i>
        `);
        password.type = "password";

    } else {
        div.insertAdjacentHTML('beforeend', `
            <i class="fas fa-eye" onclick="seePassword()"></i>
        `);
        password.type = "text";
    }
    record_see = !record_see;
}

function signIn() {
    let loader = document.getElementById("logining");
    loader.setAttribute("style", "display: block");

    const email = document.getElementById("email");
    const password = document.getElementById("password");

    const dict = {
        "auth/invalid-email": "email 不符合規定！",
        "auth/user-disabled": "該email已被停用！",
        "auth/user-not-found": "找不到該使用者！",
        "auth/wrong-password": "密碼錯誤！"
    };

    firebase.auth().signInWithEmailAndPassword(email.value, password.value)
        .then((userInfo) => {
            db.collection("Profile").doc(userInfo.user.uid).get().then((doc) => {
                //console.log(JSON.stringify(doc.data()));
                localStorage.setItem("profile_id", doc.id);
                db.collection('Profile').doc(doc.id).update({
                    isOnline: true
                }).then(() => {
                    loader.setAttribute("style", "display: none");
                    location.href = "./ChatList.html"; //"http://localhost:5500/ChatList.html";
                });
            });
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            loader.setAttribute("style", "display: none");
            alert(dict[errorCode]);
            //console.log(errorCode + " " + errorMessage);
        });
}

function otherWaySignIn(way) {
    let loader = document.getElementById("logining");
    loader.setAttribute("style", "display: block");

    var provider = (way === "facebook") ? new firebase.auth.FacebookAuthProvider() : new firebase.auth.GoogleAuthProvider();
    //Popup
    firebase.auth().signInWithPopup(provider)
        .then(function (result) {
            // This gives you a Facebook/Google Access Token. You can use it to access the Facebook/Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            let profile_ref = db.collection('Profile');

            //console.log(user);
            profile_ref.doc(user.uid).get()
                .then((result) => {
                    if (!result.exists) //不存在
                    {
                        profile_ref.doc(user.uid).set({
                            birth_date: "",
                            createTime: date,
                            email: user.email,
                            friends: [],
                            head_pic: defaultuserpic,
                            last_online_time: "",
                            notifications: [],
                            name: user.displayName,
                            isOnline: true,
                            background_pic: "",
                            discription: ""
                        }).then(() => {
                            alert("註冊成功！");
                            localStorage.setItem("profile_id", user.uid);
                            db.collection('Profile').doc(user.uid).update({
                                isOnline: true
                            }).then(() => {
                                loader.setAttribute("style", "display: none");
                                location.href = "./ChatList.html"; //"http://localhost:5500/ChatList.html";
                            });
                        });
                    } else //存在
                    {
                        localStorage.setItem("profile_id", user.uid);
                        db.collection('Profile').doc(user.uid).update({
                            isOnline: true
                        }).then(() => {
                            loader.setAttribute("style", "display: none");
                            location.href = "./ChatList.html"; //"http://localhost:5500/ChatList.html"; //"https://1ab4-122-254-13-24.ngrok.io/ChatList.html";       
                        });
                    }
                });
        }).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            loader.setAttribute("style", "display: none");
            //alert(errorMessage);
            alert("帳號已存在！"); //auth/account-exists-with-different-credential
            //console.log(errorCode + " " + errorMessage);
        });
}